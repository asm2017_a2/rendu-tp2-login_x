unsigned power(unsigned a, unsigned b) {
    if (b == 0) return 1;
    return a * power(a, b - 1);
}

unsigned quick_power(unsigned a, unsigned b) {
    unsigned r = 1;
    if (b > 0) {
        r = quick_power(a * a, b / 2);
        if ( b % 2 != 0)
            r *= a;
    }
    return r;
}

unsigned power_c(unsigned a, unsigned b, unsigned *count)
{

}

unsigned quick_power_c(unsigned a, unsigned b, unsigned *count)
{

}

int main() {
    unsigned a = 2, b = 15;
    unsigned c0 = 0, c1 = 0;

    printf("power_c(%u, %u, &c0) = %u\n", a, b, power_c(a, b, &c0));
    printf("  c0 = %u\n", c0);
    printf("quick_power_c(%u, %u, &c1) = %u\n", a, b, quick_power_c(a, b, &c1));
    printf("  c1 = %u\n", c1);

    return 0;
}
