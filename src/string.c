/*
 * change in place every lower case letter to the corresponding upper case
 * letter.
 */
void to_upper(char *s)
{

}

/*
 * apply in place the rot13 translation
 * preserves lower and upper case
 */
void rot13(char *s)
{

}

/*
 * For more precise definition of these functions read their man pages.
 * (note mystrlen is strlen(3) and so on ... )
 */

// string length
size_t mystrlen(const char *s)
{

}

// copy atmost n bytes from src to dest
// 0 pad dest if src contains less than n bytes
char *mystrncpy(char *dest, const char *src, size_t n)
{

}

// copy atmost n bytes of src at the end of dest
// always add the final 0 at the end of dest
char *mystrncat(char *dest, const char *src, size_t n)
{

}

// compare lexicographically atmost n bytes of s1 and s2
// returns: 0 equal, negative s1 is smaller, positive otherwise
int mystrncmp(const char *s1, const char *s2, size_t n)
{

}

// returns the pointer to first occurrence of c in s
// returns NULL if c is not found
char *mystrchr(const char *s, int c)
{

}

// Same as above but return the last occurrence
char *mystrrchr(const char *s, int c)
{

}

// returns a copy of s allocated with malloc(3)
char *mystrdup(const char *s)
{

}
