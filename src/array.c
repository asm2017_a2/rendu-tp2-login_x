# define _XOPEN_SOURCE 500

# include <stdlib.h>

/*
 * allocate and fill an array of size int
 */
int* create_random_array(size_t size) {
    int *tab;
    tab = malloc(size * sizeof (int));
    for (size_t i = 0; i < size; ++i)
        // use % 256 to avoid big values
        tab[i] = random() % 256;
    return tab;
}

/*
 * compute the sum of the element of the given array
 */
int array_sum(int tab[], size_t size)
{

}

/*
 * reverse, in place, the array
 * you can reuse the swap function
 */
void array_reverse(int tab[], size_t size)
{

}

/*
 * binary search: use a dichotomic algorithm to search for the value x in a
 * sorted array tab.
 * Returns a pointer to the cell containing the searched value, or NULL if not
 * found.
 * You should search between the left (included) and right (excluded) bounds.
 */
int* binary_search(int x, int tab[], size_t left, size_t right)
{

}
