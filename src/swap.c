#include <stdio.h>
#include <stdlib.h>

void swap(int *a, int *b)
{
    // FIX ME
}

int main()
{
    int x = 1, y = 2;
    // will print: x = 1 and y = 2
    printf("x = %d and y = %d\n", x, y);
    // swap
    swap(&x, &y);
    // should print: x = 2 and y = 1
    printf("x = %d and y = %d\n", x, y);

    return 0;
}
