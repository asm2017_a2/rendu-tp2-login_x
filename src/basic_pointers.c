/* basic_pointers.c */

# include <stdio.h>
# include <stdlib.h>


int main() {
    // Simple integer
    int           x = 42;
    // A pointer to an integer
    int          *p;

    // Referencing: take the address of a left value
    p = &x;

    // Print the value of x
    printf("x = %d\n", x);

    // Dereferencing: follow the pointer to access the stored value
    printf("*p = %d\n", *p);

    // Dereferencing can be used to modify the
    // pointed value
    *p = 0;

    printf("x = %d\n", x);
    printf("*p = %d\n", *p);

    // Pointers are values
    int          *q;
    // q contains the same address as
    // p
    q = p;

    // Print the address in p,
    // then the referenced value
    printf("p : %p (%d)\n", p,
            *p);

    // Print the address in
    // q, then the referenced
    //    value
    printf("q : %p (%d)\n", q, *q);

    return 0;
}
